# Deep Dive Typescript

## About this project

* Create-react-app (JS Template)
* Removed some junk such as assets and web tracking.
* Added recommended [tsconfig.json](https://www.npmjs.com/package/@tsconfig/recommended).
  * With some annotations
* Add typescript package.

## Activity 1 - JS Conversion

* Enable typescript `mv example.tsconfig.json tsconfig.json`
* Convert the application to use 

## Activity 2 - Runtime type checking

* Add type checking to the API responses to ensure the schema matches what is expected

Example using https://github.com/gcanti/io-ts:

```
// Before

type FindArtistResponse = {
  meta: {
    query: string;
    total: number;
    perPage: number;
    page: number;
    totalPages: number;
  };
  result: {
    type: 'artist';
    name: string;
    listeners: number;
  }[];
};

// After 

import * as t from 'io-ts';

const FindArtistResponse = t.type({
  meta: t.type({
    query: t.string,
    total: t.number,
    perPage: t.number,
    page: t.number,
    totalPages: t.number,
  }),
  result: t.array(
    t.type({
      type: t.literal("artist"),
      name: t.string,
      listeners: t.number,
    })
  ),
});
type FindArtistResponse = t.TypeOf<typeof FindArtistResponse>;

export const FindArtist = (artist): Promise<FindArtistResponse> => {
  doSearch(artist).then((data) => {
    const result = FindArtistResponse.decode(data);
    if (result._tag === 'Right') {
      resolve(result);
    } else {
      reject(new Error('Unexpected schema'));
    }
  });
};

```