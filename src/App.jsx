import React, {useState} from "react"
import {Search} from "./components/Search";
import {FindArtist, FindSong} from "./service/MusicFinder";
import {useLoading} from "./hooks/useLoading";
import {KeyboardEnabledList} from "./components/KeyboardEnabledList";
import {Artist} from "./components/Artist";
import {Song} from "./components/Song";

function App() {
    const [artistResults, setArtistsResults] = useState()
    const [songResults, setSongResults] = useState()
    const [error, setError] = useState()
    const [isLoading, promiseIsLoading] = useLoading()
    const onSearch = (query, type) => promiseIsLoading(() => {
      setArtistsResults()
      setSongResults()
      return type === 'artist'
              ? FindArtist(query).then(setArtistsResults).catch(setError)
              : FindSong(query).then(setSongResults).catch(setError)
        }
    )

    return (
        <div className="App">
            <Search onSearch={onSearch}/>
            {isLoading && <span>Loading ...</span>}
            {error && <pre>{JSON.stringify(error)}</pre>}
            {artistResults && <KeyboardEnabledList>{artistResults.result.map((artist, i) =>
                <Artist key={i} artist={artist}/>)}</KeyboardEnabledList>}
            {songResults && <KeyboardEnabledList>{songResults.result.map((song, i) =>
                <Song key={i} song={song}/>)}</KeyboardEnabledList>}
        </div>
    );
}

export default App;
