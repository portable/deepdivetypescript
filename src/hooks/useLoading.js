import {useState} from "react";

export const useLoading = () => {
    const [isLoading, setIsLoading] = useState(false)
    const promiseIsLoading = (functionReturingPromise) => {
        setIsLoading(true)
        functionReturingPromise().finally(() => setIsLoading(false))
    }
    return [
        isLoading,
        promiseIsLoading
    ]
}