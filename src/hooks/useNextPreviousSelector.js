import {useReducer, useState} from "react";

const positiveOrMax = (number, max) => {
    return number >=0 ? number : max
}

export const useNextPreviousSelector = (max) => {
    const [selected, dispatch] = useReducer((state, action) => {
        switch (action) {
            case 'next':
                return ((state ?? -1) + 1) % max
            case 'previous':
                return positiveOrMax((state ?? max) - 1, max-1)
        }
    }, undefined)
    const SelectedNext = () => dispatch('next')
    const SelectedPrevious = () => dispatch('previous')
    return [
        selected,
        SelectedNext,
        SelectedPrevious
    ]
}