import {ReadableNumber} from "./ReadableNumber";

export const Artist = ({artist, selected}) => {
  return <div style={{border: `1px solid ${selected===1 ? 'black' : 'white'}`}}>
      <span>{artist.name}</span> <ReadableNumber number={artist.listeners} /> Listeners
    </div>
}