import React, {useEffect} from "react";
import {useNextPreviousSelector} from "../hooks/useNextPreviousSelector";

export const KeyboardEnabledList = ({children}) => {
    const [selected, next, previous] = useNextPreviousSelector(children.length)
    const arrowKeyListener = e => {
        switch (e.key) {
            case 'ArrowDown':
                e.preventDefault()
                next()
                break;
            case 'ArrowUp':
                e.preventDefault()
                previous()
                break;
        }
    }
    useEffect(() => {
        document.body.addEventListener('keyup', arrowKeyListener)
        return () => {
            document.body.removeEventListener('keyup', arrowKeyListener)
        }
    }, [])

    const selectedChildren = selected !== undefined ? React.Children.map(children, (child, i) => {
        if(i === selected) {
            return React.cloneElement(child, {selected: 1})
        }
        return React.cloneElement(child, {selected: 0})
    }) : children

    return <>
        {selectedChildren}
    </>
}