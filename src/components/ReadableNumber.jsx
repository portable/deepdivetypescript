
export const ReadableNumber = ({number}) => {
    if(number > 1000000) {
        return Math.round(number/100000)/10 + " Million"
    }
    if(number > 1000) {
        return Math.round(number/1000) + " Thousand"
    }
    if(!number) {
        return null
    }
    return number
}