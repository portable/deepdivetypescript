import {useState} from "react";


export const Search = ({onSearch}) => {
    const [query, setQuery] = useState("")
    const [type, setType] = useState("artist")
    return <form onSubmit={(e) => {
        e.preventDefault()
        onSearch(query, type)
    }}>
        <input placeholder={"Search"} value={query} onChange={e => setQuery(e.target.value)} />
        <select onChange={e => setType(e.target.value)}>
            <option value={'artist'} selected={type == 'artist'}>Artist</option>
            <option value={'song'} selected={type == 'song'}>Song</option>
        </select>
        <button type={"submit"}>Submit</button>
    </form>
}