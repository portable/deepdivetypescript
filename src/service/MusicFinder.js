

export const LastFMAPI = () => {
    const LastFM = require('last-fm')

    return new LastFM('e2431c536eb106c90a08e9b4ec123980', {
        // secret: '5cf4f9a2b110a4145d53e8843b9009de',
        useragent: 'portable/v0.1 Deep Dive' // optional. defaults to lastfm-node.
    });
}

export const FindArtist = (artist) => {
    return new Promise((resolve, reject) => {
        console.log(`Search for artist: ${artist}`)
        LastFMAPI().artistSearch({q: artist}, (err, data) => {
            console.log(err, data)
            if(err) {
                reject(err)
            }
            resolve(data)
        })
    })
}
export const FindSong = (artist) => {
    return new Promise((resolve, reject) => {
        console.log(`Search for track: ${artist}`)
        LastFMAPI().trackSearch({q: artist}, (err, data) => {
            console.log(err, data)
            if(err) {
                reject(err)
            }
            resolve(data)
        })
    })
}